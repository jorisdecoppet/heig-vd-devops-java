# BUILD STAGE
FROM maven:3.9.5-eclipse-temurin-17-alpine AS build  
WORKDIR /app
COPY src ./src  
COPY pom.xml .
RUN mvn package

# PACKAGE STAGE
FROM amazoncorretto:17-alpine3.15
COPY --from=build /app/target/testing-web-complete-0.0.1-SNAPSHOT.jar /app/testing-web-complete-0.0.1-SNAPSHOT.jar  
EXPOSE 8080  
CMD ["java","-jar","/app/testing-web-complete-0.0.1-SNAPSHOT.jar"]  